import React from "react";
import { StyleSheet, Text, View, ImageBackground } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { TextInput, TouchableOpacity } from "react-native-web";

const image = { uri: "https://sciences.sorbonne-universite.fr/sites/default/files/media/2020-10/shutterstock_handicap.jpg"};

function pagePresentation() {
  return (
    <View style={styles.container}>
      <ImageBackground source={image} resizeMode="repeat" style={styles.imgBackground}>
        <LinearGradient colors={["#09203f", "#537895"]}start={[0.1, 0.1]} style={styles.linearGradient}>
          <Text style={styles.text} >Linear Gradient Example</Text>
          <TextInput></TextInput>
          <TouchableOpacity onPress={handleSignOut} style={styles.button}>
        <Text style={styles.buttonText}>Start with Application</Text>
      </TouchableOpacity>
        </LinearGradient>
      </ImageBackground>
    </View>
  );
}
const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  imgBackground: {
    flex: 1,
    width: "100%",
    alignItems: "center",
  },
  linearGradient: {
    width: '100%',
    height: '100%',
    opacity: 0.95,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: '#fff',
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  button: {
    backgroundColor: '#0782F9',
    width: '60%',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: 40,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  }
});

export default pagePresentation;
